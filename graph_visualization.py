#!/usr/bin/python

import os
import pandas as pd
import networkx as nx

# initialize object for file
raan_file = ''
# look through all files in folder for file named with 'raan' and asign object
for file in os.listdir():
    if 'raan' in file:
        raan_file = file

#obtain data frame as dictionary from all sheets in file
df = pd.read_excel(raan_file, sheet_name=None)
# get dict with edges
edges = df['edges']
# get dict with nodes dropping NaN column
nodes = df['nodes'].dropna(axis=1)

# initialize NetworkX graph with weighted edge list
G = nx.from_pandas_edgelist(edges, source='source_id', target='target_id', edge_attr='weights')
# add nodes from filtered node list
G.add_nodes_from(nodes['node_id'])
# create node positions using Fruchterman-Reingold force-directed algorithm
pos = nx.spring_layout(G)

# show plot with labels (node_id) and colors from dict
nx.draw(G, pos=pos, with_labels=True, node_color=nodes['node_color'])


