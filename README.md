# Case Study

The file 'graph_visualization.py' contains a very basic Python program that collects data from an especific excel file which must be present at the same directory in order to obtain a data visualization as a 2D network graph.

Packages & versions used:
+ Python v3.8
+ Pandas v1.2
+ NetworkX v2.5 (https://networkx.org/documentation/stable/)

Bugs & basic code improvements not yet implemented:
+ add argument parser to opt for convenient input & output file handling
+ exceptions handling
+ categorize/group code by independent functions
+ node color coding seems not to match with original data attribute
+ incorporate labels from 'node_label' column instead of 'node_id' to graph
+ visual representation might not be the most favorable to this data: different layouts & 3D visualization could give better insight on the data
